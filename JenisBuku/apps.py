from django.apps import AppConfig


class JenisbukuConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'JenisBuku'
